package main

import (
	"sort"
	"strconv"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	arr := [...]int{1, 3, 4}
	return arr
}

func ReturnIntSlice() []int {
	slice := make([]int, 0, 3)
	for i := 1; i < 4; i++ {
		slice = append(slice, i)
	}
	return slice
}

func IntSliceToString(ints []int) string {
	var result string

	for _, val := range ints {
		result = result + strconv.Itoa(val)
	}

	return result
}

func MergeSlices(float32s []float32, int32s []int32) []int {
	resultSlice := []int{}

	for _, val := range float32s {
		resultSlice = append(resultSlice, int(val))
	}
	for _, val := range int32s {
		resultSlice = append(resultSlice, int(val))
	}

	return resultSlice
}

func GetMapValuesSortedByKey(strings map[int]string) []string {
	result := make([]string, 0, len(strings))
	idxs := make([]int, 0, len(strings))
	for key := range strings {
		idxs = append(idxs, key)
	}
	sort.Ints(idxs)
	for _, idx := range idxs {
		result = append(result, strings[idx])
	}
	return result
}
